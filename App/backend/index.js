'use strict';

const firebase = require("firebase");

// コマンド実行
const execSync = require('child_process').execSync;
var commandLine = function( cmdLine ) {
   const result = execSync( cmdLine ).toString();
   console.log(result);
   return this;
}

// set a Firebase SDK snippet
const firebaseConfig = {
  apiKey: "",
  authDomain: "",
  databaseURL: "",
  projectId: "",
  storageBucket: "",
  messagingSenderId: "",
  appId: "",
  measurementId: ""
};
firebase.initializeApp( firebaseConfig );

// check firebase database
var db = firebase.database();
var ref = db.ref( "/line" );
ref.on( "child_changed", function( changedSnapshot ) {
  var key = changedSnapshot.key;
  var request = changedSnapshot.child("request").val();
  var isRequest = changedSnapshot.child("isRequest").val();

  // コマンド判定と実行
  if(key === 'message' && isRequest === true) {
    if(request === 'start') { // LINE Botに入力するテキスト
      commandLine('ls'); // 実際に実行するコマンド
    }
    ref.update({
      message:
        request: "",
        isRequest: false
      }
    });
  }
});
