# About

こちらはREADME (LINE Bot) の後に読んでください．

ffmpegをバックグラウンドで動作させるためのsystemdサービスに関する情報です．

# Unit file の設定

stream-video.service を新規で追加，または変更した場合に限り，以下のコマンドを実行してシステムに反映します．

```
sudo cp stream-video.service /usr/lib/systemd/user/
systemctl --user daemon-reload
```

# 設定ファイル

stream-video-env が設定ファイルです．/etc/default/にコピーしてください．

```
sudo cp stream-video-env /etc/default/
```

設定を変更します．

```
sudo vi /etc/default/stream-video-env
```

設定内容は以下です．

```
# フォーマット
FORMAT=yuyv422
# 解像度(サイズ指定)
VIDEO_SIZE=1280x720
# フレームレート
FRAMERATE=30
# デバイス
DEVICE=/dev/video0
# 映像ビットレート
VIDEO_BITRATE=768k
# 音声ビットレート
AUDIO_BITRATE=128k
# サーバ
SERVER='srt://193.168.100.10:8080'
# ストリームID
STREAMID='uplive.sls.com/live/test'

```

# 起動/停止方法

## 起動

以下のコマンド実行で ~.service を起動します．

```
systemctl --user start stream-video
```

以下のコマンドで起動状態を確認できます。以下のように Active と表示されていれば正常に起動しています。

```
$ systemctl --user status stream-video
● stream-video.service - Stream Video
   Loaded: loaded (/usr/lib/systemd/user/stream-video.service; disabled; vendor preset: enabled)
   Active: active (running) since Sun 2020-11-08 10:04:32 JST; 1min 32s ago
 Main PID: 12724 (ping)
   CGroup: /user.slice/user-1000.slice/user@1000.service/stream-video.service
....

```

## 停止

以下のコマンド実行で ~.service を停止します。

```
systemctl --user stop stream-video
```

## ログの確認

以下のコマンド実行し．ログを確認できます．

```
journalctl --user -u  stream-video
```

# LINEからの操作方法

App/backend/index.js 内のコマンド実行部分に

```
systemctl --user start stream-video
```

と設定することで，LINEからffmpegのサービスを起動できます．
